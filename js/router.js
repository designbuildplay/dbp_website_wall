
// ========
// ROUTER : 
// ========

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES :::::::::::::::::::

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        HomeGrid      = require('view.homegrid'),
        LabGrid      = require('view.lab'),
        InfoView      = require('view.info'),
        ContactView   = require('view.contact'),
        ProjectView   = require('view.project'),
        EXProjectView   = require('view.experiment'),


        Project       = require('model.project'),
        PrjCollection = require('collections.projects'),
        EXPPrjCollection = require('collections.exprojects'),

        NavView      = require('view.navbar');
    
    var nav_view, info_view, view_home, view_lab, contact_view, project_view;
    // var menu_view = {};
    // var game_view = {};

   //define router class
    var AppRouter = Backbone.Router.extend ({
        routes: {
            '' : 'home',
            'home' : 'home',
            'info': 'info',
            'project/:id': 'project',
            'lab': 'lab',
            'experiment/:id': 'experiment',
            'contact': 'contact',
        },

        home: function () {
            console.log('you are viewing home page');

            if(view_home){
                view_home.remove();
            }
            
            view_home = new HomeGrid({});
            nav_view = new NavView(); // creates the NAV BAR
        },

        info: function () {
            console.log('you are viewing INFO page');
            
            if(!view_home){
                 // Create the project models ============
                 view_home = new HomeGrid({});
            }
            
            // Create the project models ============
            info_view = new InfoView();
            nav_view = new NavView(); // creates the NAV BAR
        },
        
        project: function (id) {
            console.log('you are viewing Projects');
            
            if(!view_home){
                 // Create the project models ============
                 view_home = new HomeGrid({});
            }
            
            if(id){
                var prjModel = PrjCollection.get(id)
                project_view = new ProjectView({ model:prjModel });
            }
           
           nav_view = new NavView(); // creates the NAV BAR
        },

         lab: function (id) {
            
            if(view_lab){
                view_lab.remove();
            }
            
           view_lab = new LabGrid({});
           nav_view = new NavView(); // creates the NAV BAR
        },

        experiment: function (id) {
           console.log('you are viewing the lab');

            if(id){
                var prjModel = EXPPrjCollection.get(id)
                project_view = new EXProjectView({ model:prjModel });
            }

            nav_view = new NavView(); // creates the NAV BAR
        },

       

        contact: function () {
            console.log('you are viewing CONTACT');

            if(!view_home){
                 // Create the project models ============
                 view_home = new HomeGrid({});
            }

            contact_view = new ContactView();
            nav_view = new NavView(); // creates the NAV BAR
        },



        initialize: function(){
            //console.log("route this thing")
        }

    });

  return AppRouter

});

