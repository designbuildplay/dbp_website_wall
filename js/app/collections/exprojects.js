define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Project     = require('model.exproject');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjCollection = Backbone.Collection.extend({
      model:Project,
      url:"/"
    });

    var Projects = new PrjCollection([
          {id: 1, title: "experiement 1", desc: "a summary description", img_thumb:"imgs/exp1.jpg", iframe:"http://designbuildplay.co.uk/projects/canvas/skillballs/",  link:"", techspec:[]},          
          {id: 2, title: "experiement 2", desc: "a summary description", img_thumb:"imgs/test3.jpg", iframe:"http://www.designbuildplay.co.uk/projects/stevenseagull/game.html",  link:"", techspec:[]},
         {id: 3, title: "experiement 3", desc: "a summary description", img_thumb:"imgs/test3.jpg", iframe:"http://designbuildplay.co.uk/projects/stevenseagull/rift/",  link:"", techspec:[]},
     ]); 


    // Return the model for the module
    return PrjCollection, Projects;


});