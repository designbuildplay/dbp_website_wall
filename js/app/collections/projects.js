define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Project     = require('model.project');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjCollection = Backbone.Collection.extend({
      model:Project,
      url:"/"
    });

    var Projects = new PrjCollection([
      { id: 1, title: "websockets node game", desc: "a short homepage description", img_thumb:"imgs/test1.jpg", img_large1:"imgs/prj1/large1.jpg", img_large2:"imgs/prj1/large2.jpg", img_large3:"imgs/prj1/large3.jpg", desc_long:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat dolor sed risus vulputate viverra. Morbi sed est tempor, porttitor elit vel, ultricies erat. Nullam bibendum turpis non mi porttitor, sed sollicitudin nibh tempus. Phasellus ut metus pellentesque, egestas tor", techspec:["HTML5","JAVASCRIPT","SASS"], link:"http://www.designbuildplay.co.uk"},
      {id: 2, title: "disney's frozen", desc: "a summary description",  img_thumb:"imgs/test2.jpg", desc_long:"Lorem ipsum Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat dolor sed risus vulputate viverra. Morbi sed est tempor, porttitor elit vel, ultricies erat. Nullam bibendum turpis non mi porttitor, sed sollicitudin nibh tempus. Phasellus ut metus pellentesque, egestas tor dolor sit amet, consectetur adipiscing elit. Aliquam volutpategestas tor", techspec:["HTML5"]  },
      {id: 3, title: "Burberry", desc: "a summary description", img_thumb:"imgs/test3.jpg", desc_long:"a ssad",  techspec:[]},
      {id: 4, title: "WAAA", desc: "a summary description", img_thumb:"imgs/test4.jpg", desc_long:"a sdas", techspec:[]},
      {id: 5, title: "Somefing COOL", desc: "a summary description", img_thumb:"imgs/test5.jpg", desc_long:"as dad",  techspec:[]},
      {id: 6, title: "Bitch YO", desc: "a summary description", img_thumb:"imgs/test6.jpg", desc_long:" ad a", techspec:["Science Bitch!"]},
    ]); 


    // Return the model for the module
    return PrjCollection, Projects;


});