define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore');

    // CONTENT :::::::::::::::::::::::::::::::::::
    
    var Project = Backbone.Model.extend({

      defaults: {
        id: "", 
        title:"", 
        desc:"", 
        img_thumb:"",
        img_large1:"",
        img_large2:"",
        img_large3:"",
        link:""
      },
      
      initialize: function(){
        
        console.log(this.get('title') + ' has been initialized.');
        
        // listens for change update  
        this.on('change', function(){
            console.log(this.get('title') + '- Values for this model have changed.');
        });
    }

    });

    return Project

});