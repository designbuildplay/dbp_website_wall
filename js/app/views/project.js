// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!templates/prj_full.html"),

        foundation  = require('foundation.core'),
        foundation_orbit = require('foundation.orbit'),
        row         = require('row'),
        grid        = require('grid'),
        tiler       = require('tiler'),
        tweenMax    = require('tweenMax'),
        greenProp    = require('GreenProp');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var HomeGrid = Backbone.View.extend({
        //collection: Projects,
        tagName:'div',
        id:"homegrid",    
        el:'#overlay',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click #close-prj': 'closeClick',
            // 'keypress .edit': 'updateOnEnter',
            // 'blur .edit':   'close'
        },

        initialize:function () {
                //console.log("view active " +  PrjCollection.models[0].get("title") )
                this.render();
        },
      
       closeClick:function(){
          $("html, body").animate({ scrollTop: 0 }, 600);
          $( "#fullbg" ).css("display","none");
          $( "#overlay" ).css("display","none");

          var Router = require('router'),
          appRouter = new Router(); 
          appRouter.navigate('', { }); // TRIGGERS THE PAGE FROM ROUTER
        },


        render:function () {

            this.$el.html(this.template( this.model.toJSON() ));
            $( "#fullbg" ).css("display","inline")
            $( "#overlay" ).css("display","inline")
            
            TweenMax.set('#prj-sect', {scale:0.7, alpha:0 });
            TweenMax.to('#prj-sect', 0.5, {scale:1, alpha:1, delay:0.2} );

            // Create the techspec list
             var techitem = this.model.get("techspec")
             for (var i = 0; i < techitem.length; i++) {
                 $("#techlist").append("<li>" + techitem[i] +"</li>")
             };
            //console.log("list" + techitem.length)
             
             $("#techlist li").hover(
                function () {
                  $(this).addClass("li_hover3");
                },
                function () {
                   $(this).removeClass("li_hover3");
                }
              );

             // ORBIT SLIDER :::::::::::::::::::::::::::::::::
            $('#imgSlider').foundation('orbit', {  
                  animation: 'slide', // Sets the type of animation used for transitioning between slides, can also be 'fade'
                  timer_speed: 10000, // Sets the amount of time in milliseconds before transitioning a slide
                  pause_on_hover: true, // Pauses on the current slide while hovering
                  resume_on_mouseout: false, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
                  next_on_click: true, // Advance to next slide on click
                  animation_speed: 500, // Sets the amount of time in milliseconds the transition between slides will last
                  stack_on_small: false,
                  navigation_arrows: true,
                  slide_number: false,
                  slide_number_text: 'of',
                  container_class: 'orbit-container',
                  stack_on_small_class: 'orbit-stack-on-small',
                  next_class: 'orbit-next', // Class name given to the next button
                  prev_class: 'orbit-prev', // Class name given to the previous button
                  timer_container_class: 'orbit-timer', // Class name given to the timer
                  timer_paused_class: 'paused', // Class name given to the paused button
                  timer_progress_class: 'orbit-progress', // Class name given to the progress bar
                  slides_container_class: 'orbit-slides-container', // Class name given to the 
                  bullets_container_class: 'orbit-bullets',
                  slide_selector: 'li', // Default is '*' which selects all children under the container
                  bullets_active_class: 'active', // Class name given to the active bullet
                  slide_number_class: 'orbit-slide-number', // Class name given to the slide number
                  caption_class: 'orbit-caption', // Class name given to the caption
                  active_slide_class: 'active', // Class name given to the active slide
                  orbit_transition_class: 'orbit-transitioning',
                  bullets: false, // Does the slider have bullets visible?
                  circular: true, // Does the slider should go to the first slide after showing the last?
                  timer: false, // Does the slider have a timer visible?
                  variable_height: true, // Does the slider have variable height content?
                  swipe: true,
                  //before_slide_change: noop, // Execute a function before the slide changes
                  //after_slide_change: noop // Execute a function after the slide changes
            });
            $('.orbit-container').css('height', '100%')
            


           return this;

      }
    });

  


    // Our module now returns our view
    return HomeGrid;


});
