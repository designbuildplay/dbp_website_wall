// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!templates/prj_exp.html"),

        foundation  = require('foundation.core'),
        foundation_orbit = require('foundation.orbit'),
        row         = require('row'),
        grid        = require('grid'),
        tiler       = require('tiler'),
        tweenMax    = require('tweenMax'),
        greenProp    = require('GreenProp');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var HomeGrid = Backbone.View.extend({
        //collection: Projects,
        tagName:'div',
        id:"homegrid",    
        el:'#overlay',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click #close-prj': 'closeClick',
            // 'keypress .edit': 'updateOnEnter',
            // 'blur .edit':   'close'
        },

        initialize:function () {
                //console.log("view active " +  PrjCollection.models[0].get("title") )
                this.render();
        },
      
       closeClick:function(){
          $("html, body").animate({ scrollTop: 0 }, 600);
          $( "#fullbg" ).css("display","none");
          $( "#overlay" ).css("display","none");

          var Router = require('router'),
          appRouter = new Router(); 
          appRouter.navigate('', { }); 
        },


        render:function () {

            this.$el.html(this.template( this.model.toJSON() ));
            $( "#fullbg" ).css("display","inline")
            $( "#overlay" ).css("display","inline")
            
            TweenMax.set('#prj-sect', {scale:0.7, alpha:0 });
            TweenMax.to('#prj-sect', 0.5, {scale:1, alpha:1, delay:0.2} );

            // Create the techspec list
             var techitem = this.model.get("techspec")
             for (var i = 0; i < techitem.length; i++) {
                 $("#techlist").append("<li>" + techitem[i] +"</li>")
             };
            //console.log("list" + techitem.length)
             
             $("#techlist li").hover(
                function () {
                  $(this).addClass("li_hover3");
                },
                function () {
                   $(this).removeClass("li_hover3");
                }
              );


           return this;

      }
    });

  


    // Our module now returns our view
    return HomeGrid;


});
