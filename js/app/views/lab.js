// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!templates/home.html"),

        row         = require('row'),
        grid        = require('grid'),
        tiler       = require('tiler'),
        tweenMax    = require('tweenMax'),
        greenProp    = require('GreenProp'),
        draggable   = require('draggable'),
        throwPlugin = require('throw'),

        Project       = require('model.exproject'),
        PrjCollection = require('collections.exprojects');

    var wallPos = document.getElementById("wall"),
        getX, tiler;

    var tileSizeUpdate = 152;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var HomeGrid = Backbone.View.extend({
        //collection: Projects,
        tagName:'div',
        id:"homegrid",    
        el:'#viewport',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click .tile': 'clickTag',
        },

      initialize:function () {
                //console.log("view active " +  PrjCollection.models[0].get("title") )
                this.render();
      },
      

      clickTag:function(){
               //console.log("clicky")
      },

      
      render:function () {

          $(window).resize(resize).trigger("resize"); //trigger calls the function iniatally
          window.onorientationchange = resize;
          $('.info-sect' ).removeClass("infoIn");
          $( "#fullbg" ).css("display","none");
          $( "#overlay" ).css("display","none");
           
         //getX = wallPos._gsTransform.x; //gets Getter for X Pos
         function renderGrid(){

          if($("#viewport").length == 0) {
              //it doesn't exist
              $("body").append("<div id='viewport'><div id='wall'></div>")
            }
            else{
             $("#wall").html("")
            }

           // RENDER THE GRID ::::::::::::::::::::::::::::::::
                var collectionSize = PrjCollection.length;
                //console.log("coll length is " + collectionSize)

                  tiler = new Tiler($('#wall'), {
                  tileSize: tileSizeUpdate,

                  fetch: function(tofetch) {

                    tofetch.forEach(function(tile) {
                      
                      var img = new Image();
                      var x = tile[0];
                      var y = tile[1];
                      var imgnum  = Math.floor((Math.random()*collectionSize));
                      var block = document.createElement('div');
                      var titletab = document.createElement('div');
                      var tab = document.createElement('div');
                      var fadetime = Math.random() * 1; // random fade in time
                      fadetime = Math.round(fadetime*10)/10
                      //img.src = 'imgs/test' + imgnum + '.jpg';
                      img.src = PrjCollection.models[imgnum].get("img_thumb")

                     $(img).addClass("tileImg")

                     $( block ).addClass( "tile" );
                     $( block ).css( "width", tileSizeUpdate );
                     $( block ).css( "height", tileSizeUpdate );

                     $( titletab ).addClass( "title-info" );
                     $( tab ).addClass( "tab-info" );

                     $( block ).append(img);
                     $( block ).append(titletab);
                     $( block ).append(tab);

                     $( tab ).append("view project <div class='navRing'> > </div>"); // ADDS THE TITLE
                     $( titletab ).append(PrjCollection.models[imgnum].get("title")); // ADDS THE TITLE

                     //console.log(titletab)
                     
                     $(tab).click(function() {
                        
                        var blockId  = PrjCollection.models[imgnum].get("id")

                        console.log("hit ", blockId)

                        var Router = require('router'),
                        appRouter = new Router(); 
                        appRouter.navigate('experiment/' + blockId, {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER

                      });
                        
                     // HOVER CLASS ::::::::::::::::
                     $(block).hover(
                        function () {
                          $(this).children( '.tab-info' ).addClass("tab-in");
                          $(this).children( '.title-info' ).addClass("title-in");
                           $(this).children( img ).addClass("img-in");
                        },
                        function () {
                           $(this).children( '.tab-info' ).removeClass("tab-in");
                           $(this).children( '.title-info' ).removeClass("title-in");
                           $(this).children( img ).removeClass("img-in");
                        }
                      );

                     // FADE IN AND SHOW THE TILE ::::::::::::::::

                  
                    // TweenMax.set(img, {scale:0.2, alpha:0 });
                    // TweenMax.to(img, fadetime, {scale:1, alpha:1, delay:fadetime/2, overwrite:true} );
                    // TweenMax.to(block, fadetime, {alpha:1});

                  //TweenMax.to(block, 0.3, { onComplete:tileIn });
                    tiler.show(x, y, block); //displays the block
                    //console.log(fadetime)

                    });

                     function tileIn(){
                          $('.tile').addClass('tileIn')
                     }
                  }
             });

             tiler.refresh();

             Draggable.create("#wall", {type: "y,x", edgeResistance:0.5, throwProps: true, onDrag:refresh, onThrowUpdate:refresh, overwrite:true});
            
      }
      function refresh(){

           
            // tiler.refresh();

            if(tiler){
              tiler.refresh();
            }
              
              //console.log('refresh tiles')
              // getX = wallPos._gsTransform.x;
                    
       }

      function loadTiles(){
             var w = $( window ).width();
               
              if(w < 480){
                 tileSizeUpdate = 75
              }
              else if(w < 640){
                tileSizeUpdate = 100

              }

              else if(w < 970){
                tileSizeUpdate = 150
              }
              else{
                tileSizeUpdate = 200
               
              }

             renderGrid()
             // console.log(tileSizeUpdate)
      }

      function resize(){
           
           TweenMax.to('#wall', 0.1, { alpha:1, delay:0.05, overwrite:true, onComplete:loadTiles} );
     
        }


         $('#wall').mousemove(function(event) {
            // var cx = Math.ceil($('body').width() / 2.0);
            // var cy = Math.ceil($('body').height() / 2.0);
            // var dx = event.pageX - cx;
            // var dy = event.pageY - cy;

            // var tiltx = (dy / cy);
            // var tilty = - (dx / cx);
            // var radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
            // var degree = (radius * 20);
            // TweenLite.set("#wall", {transform:'rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)'});
    //        $('#picture').css('-webkit-transform','rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)');
        });

      }
    });

 
    // Our module now returns our view
    return HomeGrid;


});
