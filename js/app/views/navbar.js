// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!templates/navbar.html");


    // CONTENT :::::::::::::::::::::::::::::::::::

    var NavView = Backbone.View.extend({
        //collection: Projects,
        tagName:'header',
        id:"navbar",    
        el:'#nav',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click #nav0': 'logoClick',
            'click #nav1': 'infoClick',
            'click #nav2': 'experiClick',
            'click #nav3': 'contactClick',
            'click #brand': 'logoClick',
            //'click #close-info': 'closeClick',
        },

        initialize:function () {
                //console.log("view active " +  PrjCollection.models[0].get("title") )
                this.render();
        },
      
       infoClick:function(){
          var Router = require('router'),
              appRouter = new Router(); 

            appRouter.navigate('info', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
        },

        experiClick:function(){
          var Router = require('router'),
              appRouter = new Router(); 

            appRouter.navigate('lab', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
            // $( "#overlay" ).css("display","none");
            // $('.info-sect' ).removeClass("infoIn");
        },

        contactClick:function(){
          var Router = require('router'),
              appRouter = new Router(); 

            appRouter.navigate('contact', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
        },

        logoClick:function(){
            var Router = require('router'),
              appRouter = new Router(); 

            appRouter.navigate('home', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
            $( "#overlay" ).css("display","none")
            $('.info-sect' ).removeClass("infoIn");
        },


        render:function () {
           
           this.$el.html(this.template(  ));
            // return this;
           //console.log("nav createed")

                  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                  // ROLLOVER animation class effects :::::::::::::::::::::::::::::::::


                    $(".brand").hover(
                      function () {
                        $(this).children( '.logo' ).addClass("logo_hover");
                        $(this).children( '.subtext2' ).addClass("show");
                      },
                      function () {
                        $(this).children( '.logo' ).removeClass("logo_hover");
                        $(this).children( '.subtext2' ).removeClass("show");
                      }
                    );

                      $(".logo-tweet").hover(
                      function () {
                        $(this).addClass("icon_hover");
                      },
                      function () {
                        $(this).removeClass("icon_hover");
                      }
                    );

                      $(".logo-github").hover(
                      function () {
                        $(this).addClass("icon_hover");
                      },
                      function () {
                        $(this).removeClass("icon_hover");
                      }
                    );

                     $(".navitem").hover(
                      function () {
                        $(this).children( '.navRing' ).addClass("ring_hover");
                      },
                      function () {
                        $(this).children( '.navRing' ).removeClass("ring_hover");
                      }
                    );


                     $(".close").hover(
                      function () {
                        $(this).children( '.navRing' ).addClass("ring_hover");
                      },
                      function () {
                        $(this).children( '.navRing' ).removeClass("ring_hover");
                      }
                    );

                     $(".contact").hover(
                      function () {
                        $(this).children( '.navRing' ).addClass("ring_hover");
                      },
                      function () {
                        $(this).children( '.navRing' ).removeClass("ring_hover");
                      }
                    );

                    $("#skills1 li").hover(
                      function () {
                        $(this).addClass("li_hover1");
                      },
                      function () {
                         $(this).removeClass("li_hover1");
                      }
                    );

                     $("#skills2 li").hover(
                      function () {
                        $(this).addClass("li_hover2");
                      },
                      function () {
                         $(this).removeClass("li_hover2");
                      }
                    );

                     $("#skills3 li").hover(
                      function () {
                        $(this).addClass("li_hover3");
                      },
                      function () {
                         $(this).removeClass("li_hover3");
                      }
                    );
              }
    });



    // Our module now returns our view
    return NavView;


});
