// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!templates/contact.html"),

        row         = require('row'),
        grid        = require('grid'),
        tiler       = require('tiler'),
        tweenMax    = require('tweenMax'),
        greenProp    = require('GreenProp');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var HomeGrid = Backbone.View.extend({
        //collection: Projects,
        tagName:'div',
        id:"homegrid",    
        el:'#overlay',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click #close-info': 'closeClick',
            // 'keypress .edit': 'updateOnEnter',
            // 'blur .edit':   'close'
        },

        initialize:function () {
                //console.log("view active " +  PrjCollection.models[0].get("title") )
                this.render();
        },
      
       closeClick:function(){

          $('.info-sect' ).removeClass("infoIn");
          $( "#fullbg" ).css("display","none");
          $( "#overlay" ).css("display","none");
          
          var Router = require('router'),
          appRouter = new Router(); 
          appRouter.navigate('', { }); 
        },


        render:function () {

            this.$el.html(this.template(  ));
            
            $( "#fullbg" ).css("display","inline");
            $( "#overlay" ).css("display","inline")
            $('.info-sect' ).addClass("infoIn");

           $('body').mousemove(function(event) {
             //  var cx = Math.ceil($('body').width() / 2.0);
             //  var cy = Math.ceil($('body').height() / 2.0);
             //  var dx = event.pageX - cx;
             //  var dy = event.pageY - cy;

             //  var tiltx = (dy / cy);
             //  var tilty = - (dx / cx);
             //  var radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
             //  var degree = (radius * 20);
             //  TweenLite.set("#overlay", {transform:'rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)'});
             //  //        $('#picture').css('-webkit-transform','rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)');
             // //console.log(tiltx)
              });

           return this;

      }
    });

  


    // Our module now returns our view
    return HomeGrid;


});
