
// ==============================
// THE MAIN APPLICAIOTN LOGIC .
// ==============================

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        foundation  = require('foundation.core'),
        Router      = require('router'),
        FastClick   = require('fastclick');


    var initialize = function(){
  
    // Pass in our Router module and call it's initialize function
    //Router.initialize();
    
    $(document).foundation(); // responsive framework
    
    // BIND FASTCLICK TO THE DELAYS  ::::::::::::::::
    window.addEventListener('load', function () {
          FastClick.attach(document.body);
    }, false);

    var appRouter = new Router();  //define our new instance of router   
    Backbone.history.start();   // use # History API
    //Backbone.history.start({pushState: true, root: "/work/2014/Bravand/LovedByKids/lbk-backboned/"});   // use html5 History API
     console.log("its up")
  }


  return {
    initialize: initialize

  };

});
